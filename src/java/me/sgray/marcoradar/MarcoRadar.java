package me.sgray.marcoradar;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import mkremins.fanciful.FancyMessage;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public class MarcoRadar extends JavaPlugin {

    @Override
    public void onEnable() {
        saveDefaultConfig();
        getCommand("marcoradar").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        String cmdName = cmd.getName().toLowerCase();

        if (!cmdName.equals("marcoradar")) {
            return false;
        }

        if (args.length == 1 && args[0].equals("reload")) {
            if (!sender.hasPermission("marco.admin")) {
                return false;
            } else {
                reloadConfig();
                sender.sendMessage("Config reloaded!");
                return true;
            }
        }

        if (sender.hasPermission("marco.list")) {
            boolean showEntities = false;
            if (args.length == 1 && args[0].equals("-e") || args.length == 2 && (args[0].equals("-e") || args[1].equals("-e"))) {
                showEntities = true;
            }
            boolean filterEntities = true;
            if (args.length == 1 && args[0].equals("-a") || args.length == 2 && (args[0].equals("-a") || args[1].equals("-a"))) {
                filterEntities = false;
            }

            Collection<? extends Player> onlinePlayers = getServer().getOnlinePlayers();
            if (!onlinePlayers.isEmpty()) {
                TreeMap<Integer, Player> playerCountMap = new TreeMap<>(new Comparator<Integer>() {
                    @Override
                    public int compare(Integer a, Integer b) {
                        return a.compareTo(b);
                    }
                });
                boolean isPlayer = (sender instanceof Player);
                for (Player target : onlinePlayers) {
                    if (!isPlayer || ((Player) sender).canSee(target)) {
                        playerCountMap.put(showEntities ? getNearbyEntityCount(target, filterEntities) : -1, target);
                    }
                }

                FancyMessage message = new FancyMessage("= = = = = = = Player Radar (Online: " + onlinePlayers.size() + ") = = = = = = =");
                message.color(ChatColor.YELLOW);
                message.then("\n");
                int count = 0;
                for (Map.Entry<Integer, Player> entry : playerCountMap.descendingMap().entrySet()) {
                    Player player = entry.getValue();
                    Integer entities = entry.getKey();
                    message.then(player.getDisplayName())
                            .color(ChatColor.GREEN)
                            .tooltip(getFriendlyLocation(player.getLocation()))
                            .command("/tp " + player.getName());
                    if (showEntities) {
                        message.then(" (").color(ChatColor.YELLOW);
                        message.then(entities.toString()).color(getColorForCount(entities));
                        message.then(")").color(ChatColor.YELLOW);
                    }
                    count++;
                    if (count < playerCountMap.size()) {
                        message.then(", ");
                    }
                }
                message.send(sender);
            } else {
                if (sender.hasPermission("marco.list")) {
                    sender.sendMessage(ChatColor.GOLD + "Hmm, seems nobody is online.");
                }
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Sorry, can't do that for you.");
        }
        return true;
    }

    private int getNearbyEntityCount(Player player, boolean filter) {
        Double distance = getConfig().getDouble("count-entity-radius", 20D);
        List<Entity> nearby = player.getNearbyEntities(distance, distance, distance);
        if (filter) {
            Collections2.filter(nearby, new Predicate<Entity>() {
                @Override
                public boolean apply(Entity entity) {
                    EntityType type = entity.getType();
                    return type != EntityType.ITEM_FRAME && type != EntityType.PLAYER && type != EntityType.ARMOR_STAND;
                }
            });
        }
        return nearby.size();
    }

    private String[] getFriendlyLocation(Location loc) {
        return new String[] {
                ChatColor.GREEN + "World: " + ChatColor.WHITE + loc.getWorld().getName(),
                ChatColor.GREEN + "X: " + ChatColor.WHITE + loc.getBlockX(),
                ChatColor.GREEN + "Y: " + ChatColor.WHITE + loc.getBlockY(),
                ChatColor.GREEN + "Z: " + ChatColor.WHITE + loc.getBlockZ()
        };
    }

    private ChatColor getColorForCount(int count) {
        if (count > getConfig().getInt("threshold.severe", 100)) {
            return ChatColor.RED;
        } else if (count > getConfig().getInt("threshold.moderate", 50)) {
            return ChatColor.GOLD;
        }
        else {
            return ChatColor.GREEN;
        }
    }

}
